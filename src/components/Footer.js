import React, {Component} from "react";
import {Link} from "react-router-dom";

class Footer extends Component{
    render() {
        return(
            <footer className="fixed-bottom py-4 bg-dark text-white-50">
                <div className="container text-center">
                    <small> © 2020 Copyright: ODIC</small>
                </div>
            </footer>
        )
    }
}

export default Footer;
