
//déclaration du canvas, contexte, position ainsi que click souris
var canvas,ctx,curseurX,curseurY,mouseDown=0,terminer,boutonCouleur,boutonFin,inputVisuel,boutonTelechargement,taille, lastX, lastY, touchX, touchY;


//variables pour localiser les boutons de fins
terminer = document.querySelector('.finDessin');
boutonCouleur = document.querySelector('.control');
boutonFin = document.querySelector('.btnFin');
canvas = document.getElementById('canvas');
inputVisuel = document.getElementById('canvasVersServeur');
boutonTelechargement = document.getElementById('btnTelecharger');
taille = document.getElementById('taille');
lastX = -1;
lastY = -1;



//fonction qui sert a composer un trait à la position donnée
function dessiner(ctx,x,y,tailleTrait) {
    //on vérifie tu l'on a déja fait un trais, en réinitialisant les coordonnées
    if (lastX==-1) {
        lastX=x;
        lastY=y;
    }

    // on définit la couleur du trait
    let r=0, g=0, b=0, a=255;

    // il faut définir la couleur du trait avant de le desiner
    ctx.strokeStyle = "rgba("+r+","+g+","+b+","+(a/255)+")";

    ctx.lineCap = "round"; //pour que les lignes se rejoignent entre elles
    // on dessiner un cercle noir
    ctx.beginPath(); //commencer le tracé
    ctx.moveTo(lastX,lastY); //d'abord on se déplace dans la position précédente
    ctx.lineTo(x,y); //le nouveau trait
    ctx.lineWidth = taille.value;
    ctx.stroke();

    ctx.closePath(); //fermer le tracer
    //on met a jour les coordoonée du dernier trait pour le pronchain trait pour qu'il sache quelles coordonnée relier
    lastX=x;
    lastY=y;
}


// on vérifie que la souris est bien cliquée
function sourisOn() {
    mouseDown=1;
    dessiner(ctx,curseurX,curseurY,12);
}

// on vérifie quand la souris est relachée
function sourisOff() {
    mouseDown=0;

    //on réinitialise les coordonnées pour qu'elle ne rejoignent pas le futur nouveau trait
    lastX=-1;
    lastY=-1;
}

// on regarde la position de la souris à chaque déplacement
function sourisMove(e) {
    //on met à jour sa position
    positionSouris(e);

    // on lance la fonction pour dessiner quand on clique avec la souris
    if (mouseDown==1) {
        dessiner(ctx,curseurX,curseurY,12);
    }
}

// position de la souris par rapport à l'axe du canvas
function positionSouris(e) {
    if (!e)
        // eslint-disable-next-line no-restricted-globals
        var e = event;

    if (e.offsetX) {
        curseurX = e.offsetX;
        curseurY = e.offsetY;
    }
    else if (e.layerX) {
        curseurX = e.layerX;
        curseurY = e.layerY;
    }
}
//on gère maintenant les touchscreens
// on récupère la position du touch (action du doigt)
function touchStart() {
    //la fonction va mettre à jour els coordonnées x et y du touch
    positionTouch();

    dessiner(ctx,touchX,touchY,12); //on effectue un dessin a ce moment

    // on désactive le scroll sur mobile lors du dessin (dèq que l'on reste appuyé et qu'on trace)
    // eslint-disable-next-line no-restricted-globals
    event.preventDefault();
}

function touchEnd() {
    //la fonction va reset les coordonnées de X et Y à -1 pour les rendre invalides (on ne touche plus l'écran)
    lastX=-1;
    lastY=-1;
}

// Lance le dessin et désactive le scroll
function touchMove(e) {
    // mise à jour des coordonnées
    positionTouch(e);

    dessiner(ctx,touchX,touchY,12);

    // on désactive les comportements par défaut des évènements
    // eslint-disable-next-line no-restricted-globals
    event.preventDefault();
}


// pour récupérer la position du touch on vérifie deux choses :
// on réucpère la position X et Y sur le canvas et la position x Y sur le document
//avec l'attribut offsetLeft et offsetTop qui renvoit le nombre de pixel en décalage
function positionTouch(e) {
    if (!e)
        // eslint-disable-next-line no-restricted-globals
        var e = event;

    if(e.touches) {
        if (e.touches.length == 1) {
            var touch = e.touches[0];
            touchX=touch.pageX-touch.target.offsetLeft; //le calcul pour gérer le reste entre la position du canvas et la position X de la fenetre
            touchY=touch.pageY-touch.target.offsetTop; //le meme calcul pour l'axe Y
        }
    }
}


// On vérifie que le navigateur support la méthode getcontext pour pouvoir utiliser le canvas
if (canvas.getContext)
    ctx = canvas.getContext('2d');
//on ajuste la taille du canvas par rapport à l'déclarationcanvas.width = window.innerWidth - 60;
//on mrenplit le canvas en blanc pour ne pas laisser de transparant
canvas.width = window.innerWidth - 60;
canvas.height = window.innerHeight*0.6;
ctx.fillStyle = "white";
ctx.fillRect(0, 0, canvas.width, canvas.height);

// On insère les évènements de la souris pour le fonctionnement du canvas
if (ctx) {
    canvas.addEventListener('touchstart', touchStart, false);
    canvas.addEventListener('touchmove', touchMove, false);
    canvas.addEventListener('touchend', touchEnd, false);
    canvas.addEventListener('mousedown', sourisOn, false);
    canvas.addEventListener('mousemove', sourisMove, false);
    window.addEventListener('mouseup', sourisOff, false); //ici on cible windows pour bien récupérer le moment de relachement de la souris (si en dehors du canvas par exemple)
}



//quand le dessin est terminé on change les boutons pour affichers ceux liés à l'envoi en BDD
function swapBouton(){
    boutonCouleur.classList.add('noControl'); //on masque les boutons de controle car plus besoin
    boutonFin.classList.add('choixFin'); //on affiche les boutons de fin de dessin
    var imageEncodee = canvas.toDataURL('image/png'); //on récupère le code binaire encodé du canvas
    inputVisuel.value = imageEncodee; //la valeur de l'image sera inséré dans l'input a ce sujet
}

function confirmerFin(){
    //fonction qui confirme si l'on veut terminer notre dessin
    // eslint-disable-next-line no-restricted-globals
    let x = confirm("Vous avez terminé?");
    if (x){
        swapBouton();
    }
    else{
        return false;
    }
}



//si l'utilisateur veut télécharger son visuel, on le convertit puis on lui en donne la possibilitée
boutonTelechargement.onclick = function(){
    boutonTelechargement.download = "Illustration.jpeg";
    // eslint-disable-next-line no-undef
    allan =
        document.getElementById("btnTelecharger").href = document.getElementById("canvas").toDataURL('image/jpeg', 1.0).replace(/^data:image\/[^;]/, 'data:application/octet-stream');
}

terminer.onclick = confirmerFin;
