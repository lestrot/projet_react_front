import React,{Component} from "react";
import UsersService from "../services/UsersService";

class UserSettings extends Component{
    state = {
        isModerated: '',
        firstname: '',
        lastname: '',
        surname: '',
        email: '',
        password: ''
    };

    async componentDidMount() {
        let id = localStorage.getItem('user_id');
        let response = await UsersService.details(id);
        // console.log(response);
        if(response.ok){
            let data = await response.json();
            // console.log(data);
            this.setState({
                isModerated: data.user.isModerated,
                firstname: data.user.firstname,
                lastname: data.user.lastname,
                surname: data.user.surname,
                email: data.user.email

            });
            console.log(this.state);
        }
    }

    // listener for text input
    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    // send a request to update current user
    async submit(e){
        e.preventDefault();
        // console.log('update enregistré');
        // let response = await PostsService.auth(this.state);
        let id = localStorage.getItem('user_id');
        let body = {
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            surname: this.state.surname,
            email: this.state.email,
            password: this.state.password,
            isModerated: false
        };
        let response = await UsersService.update(id, body);
        console.log(response);
        if(response.ok){
            console.log("compte mis à jour");
        }
        // this.props.history.push('/');
    }
    render() {
        let {firstname, lastname, surname, email, password} = this.state;
        return(
            <div className={"container"}>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Prénom</label>
                        <input type="text" id={'firstname'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={firstname}/>
                    </div>

                    <div className="form-group">
                        <label>Nom</label>
                        <input type="text" id={'lastname'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={lastname} />
                    </div>
                    <div className="form-group">
                        <label>Surnom</label>
                        <input type="text" id={'surname'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={surname} />
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <input type="text" id={'email'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={email} />
                    </div>
                    <div className="form-group">
                        <label>Mot de passe</label>
                        <input type="password" id={'password'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={password} />
                    </div>

                    <button type={"submit"} className={'btn btn-primary'}>Mettre à jour le compte</button>
                </form>
            </div>
        )
    }
}

export default UserSettings;
