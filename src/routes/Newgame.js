import React,{Component} from "react";
import PartiesService from "../services/PartiesService";
import UsersService from "../services/UsersService";

class Newgame extends Component{
    state = {
        name: "",
        theme: "",
        creatorId: "",
        players: [],
        drawings: [],
        isActive: ''
    };

    // initiate game informations
    async componentDidMount() {
        let id = localStorage.getItem('user_id');
        let response = await UsersService.details(id);
        if(response.ok) {
            let data = await response.json();
            this.setState({name: data.user.surname});
        }
        localStorage.getItem('user_id') && this.setState({creatorId: localStorage.getItem('user_id')});
    }

    // listent to inputs modifications
    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    // send session request information to server
    async submit(e){
        e.preventDefault();
        console.log('process creation partie');
        let response = await PartiesService.create(this.state);
        if(response.ok){
            this.props.history.push('/findgame');
        }else{
            console.log(response);
        }
    }

    render() {
        let {name, theme} = this.state;
        return (
            <div className="container">
                <form onSubmit={(e) => this.submit(e)}>
                        <input type="hidden" id={'name'} className={'form-control'}  value={name} readOnly/>
                    <div className="form-group">
                        <label>Thème de la partie</label>
                        <input type="text" id={'theme'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={theme}/>
                    </div>
                    <button type={"submit"} className={'btn btn-primary'}>Créer la partie</button>
                </form>
            </div>
        );
    }
}

export default Newgame;
