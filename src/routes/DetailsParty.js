import React,{Component} from "react";
import PartiesService from "../services/PartiesService";
import JoinedUser from "../components/JoinedUser";
import SendedDrawing from "../components/SendedDrawing";
import {Link} from "react-router-dom";
import Party from "../components/Party";

class DetailsParty extends Component{
    state = {
        name: '',
        theme: '',
        players: [],
        drawings: [],
        creatorId: '',
        userHasJoin: ''
    };

    // define current party informations
    async componentDidMount() {
        let {id} = this.props.match.params;
        let response = await PartiesService.details(id);
        if(response.ok){
            let data = await response.json();
            this.setState({players: data.party.players, drawings: data.party.drawings, creatorId: data.party.creatorId, name: data.party.name, theme: data.party.theme});
            let players = this.state.players.map(player => player.userId);
            if(players.includes(localStorage.getItem('user_id'))){
                this.setState({userHasJoin: false});
            }
        }
        // console.log(this.state);
    }
    // function to send user request to participate
    async getIntoGame(e){
        e.preventDefault();
        let {id} = this.props.match.params;
        let body = {
            userId: localStorage.getItem('user_id'),
            partyId: id
        };
        let response = await PartiesService.joinSession(body);
        if(response.ok){
            window.location.reload();
        }
    }

    // function to let party owner send request to end current session
    async endGame(e){
        e.preventDefault();
        let body = {
            userId: localStorage.getItem('user_id'),
            partyId: localStorage.getItem('partyId')
        };
        let response = await PartiesService.endSession(body);
        if(response.ok){
            this.props.history.push(`/`);
        }else{
            console.log(response);
        }
    }
    // function to let party owner send request to hide selected user
    async moderateUser(e){
        let body = {
            playerId: e._id,
            partyId: localStorage.getItem('partyId')
        };

        let response = await PartiesService.moderateUser(body);
        if(response.ok){
            window.location = `/details/${localStorage.getItem('partyId')}`;
        }else{
            console.log(response);
        }
    }

    // function to let party owner send request to unhide selected user
    async unModerateUser(e){
        // e.preventDefault();
        let body = {
            playerId: e._id,
            partyId: localStorage.getItem('partyId')
        };
        let response = await PartiesService.unModerateUser(body);
        if(response.ok){
            window.location = `/details/${localStorage.getItem('partyId')}`;
        }else{
            console.log(response);
        }
    }

    // function to let party owner send request to hide a drawing sent
    async moderateDrawing(e){
        let body = {
            drawingId: e._id,
            partyId: localStorage.getItem('partyId')
        };

        let response = await PartiesService.moderateDrawing(body);
        if(response.ok){
            window.location = `/details/${localStorage.getItem('partyId')}`;
        }else{
            console.log(response);
        }
    }

    // function to let party owner to send a request to unhide selected drawing
    async unModerateDrawing(e){
        let body = {
            drawingId: e._id,
            partyId: localStorage.getItem('partyId')
        };

        let response = await PartiesService.unModerateDrawing(body);
        if(response.ok){
            window.location = `/details/${localStorage.getItem('partyId')}`;
        }else{
            console.log(response);
        }
    }

    render() {
        let { userHasJoin, creatorId} = this.state;
        let {id} = this.props.match.params;
        return (
            <div className="container">
                <div className="row">
                    <div className="table col-md-5">
                        <table className="table">
                            <thead>
                            <tr>
                                <th>créateur</th>
                                <th>réalisation</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.drawings.map(item => {
                                    return(
                                        (item.isModerated === true && localStorage.getItem('user_id') === creatorId)
                                            ?  <SendedDrawing data={item} creatorId={creatorId} key={item._id} unModerateDrawing={() => this.unModerateDrawing(item)}/>
                                            : (item.isModerated === true )
                                            ? item.continue
                                            :  <SendedDrawing data={item} creatorId={creatorId} key={item._id} moderateDrawing={() => this.moderateDrawing(item)}/>

                                    );
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                    <div  className="table col-md-3">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>participants</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.players.map(item => {
                                    return(
                                        (item.isModerated === true && localStorage.getItem('user_id') === creatorId)
                                            ? <JoinedUser data={item} creatorId={creatorId} key={item._id} unModerateUser={() => this.unModerateUser(item)}  />
                                            : (item.isModerated === true )
                                            ? item.continue
                                            : <JoinedUser data={item} creatorId={creatorId} key={item._id} moderateUser={() => this.moderateUser(item)}  />
                                    );
                                })
                            }
                            </tbody>
                        </table>
                        <div>
                            {(userHasJoin !== false) ? <button className={'btn btn-info'} onClick={(e) => this.getIntoGame(e)}><i className="fas fa-sign-in-alt"></i><span>rejoindre la session</span></button> : '' }
                        </div>
                    </div>
                    {/*{ (creatorId === localStorage.getItem('user_id')) ? <button  onClick={(e) => this.endGame(e)} className={'btn btn-info'}><i className="far fa-stop-circle">arrêter la session</i></button> : '' }*/}

                </div>
                <div className="row">
                    {/*{(userHasJoin !== false) ? <button className={'btn btn-info'} onClick={(e) => this.getIntoGame(e)}><i className="fas fa-sign-in-alt">rejoindre la session</i></button> : '' }*/}
                    { (creatorId === localStorage.getItem('user_id')) ? <button  onClick={(e) => this.endGame(e)} className={'btn btn-info'}><i className="far fa-stop-circle"></i>&nbsp;&nbsp;<span>arrêter la session</span></button> : '' }&nbsp;&nbsp;&nbsp;

                    {(userHasJoin === false) ? <Link to={{pathname: '/drawing', state: {partyId: id, userId: localStorage.getItem('user_id')}}} onClick={localStorage.setItem('partyId', id)} className={'btn btn-info'}><i className="fas fa-pen"></i>&nbsp;&nbsp;<span>draw</span></Link> : '' }
                </div>
            </div>

        );
    }
}


export default DetailsParty;
