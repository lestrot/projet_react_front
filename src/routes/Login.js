import React, {Component} from "react";
import UsersService from "../services/UsersService";

class Login extends Component{
    state = {
        surname: "",
        password: ""
    };

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async submit(e){
        e.preventDefault();
        console.log(this.state);
        let response = await UsersService.auth(this.state);
        if(response.ok){
            let data = await response.json();
            console.log(data);
            localStorage.setItem('token', data.token);
            localStorage.setItem('user_id', data.user._id);
            // this.props.history.push('/');
            window.location = "/";

        }else{
            console.log(response);
            console.log('raté');

        }
    }

    render() {
        let {surname, password} = this.state;
        return (
            <div className={"container"}>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Pseudo</label>
                        <input type="text" id={'surname'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={surname}/>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" id={'password'} className={'form-control'}  onChange={(e) => this.handleChange(e)} value={password}/>
                    </div>

                    <button type={"submit"} className={'btn btn-primary'}>Connexion</button>
                </form>
            </div>
        );
    }
}

export default Login;