import React, {Component} from 'react';
// import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Route} from "react-router-dom";
import Home from "./routes/Home";
import Login from "./routes/Login";
import Users from "./routes/Users";
import Register from "./routes/Register";
import UserSettings from "./routes/UserSettings";
import Newgame from "./routes/Newgame";
import FindGame from "./routes/FindGame";
import Drawing from "./routes/Drawing";
import DetailsParty from "./routes/DetailsParty";
import Header from "./components/Header";
import Footer from "./components/Footer";

class App extends Component{
    render() {
        return(
            <BrowserRouter>
                <Header />
                <Route exact path="/" component={Home} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/test" component={Users} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/account" component={UserSettings} />
                <Route exact path="/newgame" component={Newgame} />
                <Route exact path="/findgame" component={FindGame} />
                <Route exact path="/details/:id" component={DetailsParty} />
                <Route exact path="/drawing" component={Drawing} />
                <Route exact path="/drawing/:id" component={Drawing} />
                {/*<Route exact path="/register" component={Login} />*/}
                <Footer />
            </BrowserRouter>
        )
    }
}

export default App;
