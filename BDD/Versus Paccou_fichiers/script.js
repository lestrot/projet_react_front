var logo, liensNav, afficheLien;

logo = document.getElementById("logo"); //sélection du logo
lienNav = document.querySelector('.lienNav'); //sélection de la barre de navigation
logo.onclick = afficheLien; //au clic sur le logo, on masque/affiche la nav avec la fonction
function afficheLien() {
    //si n'a pas la classe lienNav => on la met
    //si a la classe lienNav => on l'enlève
    lienNav.classList.toggle('afficheLien');
};
